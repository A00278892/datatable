package com.ait.jsf;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class CartBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private ArrayList<Item> items;

	public ArrayList<Item> getItems() {
		return items;
	}

	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}
	
	public void addItemToCart(int productID, int quantity) {
		Item item = new Item(productID, quantity);
		items.add(item);
	}
	
	public void removeItemFromCart(int productID) {
		Item item = new Item(productID);
		items.remove(item);
	}
	
	public int getItemCount() {
		return items.size();
	}	

}
