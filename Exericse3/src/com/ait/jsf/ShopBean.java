package com.ait.jsf;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class ShopBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private int productID;
	private int quantity;
	
	public int getProductID() {
		return productID;
	}
	public void setProductID(int productID) {
		this.productID = productID;
	}
	
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public String addHandler() {
		CartBean cart = Helper.getBean("cartBean", CartBean.class);
		cart.addItemToCart(productID, quantity);
		return null;
	}
	
	public String removeHandler() {
		CartBean cart = Helper.getBean("cartBean", CartBean.class);
		cart.removeItemFromCart(productID);
		return null;
	}
}
