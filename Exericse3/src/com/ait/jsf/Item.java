package com.ait.jsf;

public class Item {

    private int productID;
    private int quantity;

    public Item(int productID) {
        this.productID = productID;
        this.quantity  = 0;
    }
    
    public Item(int productID, int quantity) {
        this.productID = productID;
        this.quantity  = quantity;
    }
    
    public int getProductID() {
        return productID;
    }

    public void setProductID(int itemProductID) {
        this.productID = itemProductID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int itemQuantity) {
        this.quantity = itemQuantity;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Item)) {
            return false;
        }
        Item item = (Item)obj;
        return (this.productID == item.productID);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.productID;
        return hash;
    }
}
